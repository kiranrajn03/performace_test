from locust import Locust, TaskSet, task, events
import locustrabbit



class RabbitTaskSet(TaskSet):
    @task
    def testPublish(self):
        locustrabbit.get_client().publish()

class MyLocust(Locust):
    task_set = RabbitTaskSet
    min_wait = 1000
    max_wait = 1000



def on_locust_stop_hatching():
    locustrabbit.get_client().disconnect()


events.locust_stop_hatching += on_locust_stop_hatching
