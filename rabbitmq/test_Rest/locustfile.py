from locust import HttpLocust, TaskSet, task, events


class RabbitTaskSet(TaskSet):
    @task(10)
    def get_area(self):
        self.client.get('http://oh0lw0112.intellig.local:8098/domainServices/areas')
    @task(20)
    def get_type(self):
        self.client.get('http://oh0lw0112.intellig.local:8098/domainServices/areaTypes')


#client.post("/post", {"user":"joe_hill"})


class MyLocust(HttpLocust):
    task_set = RabbitTaskSet
    min_wait = 1
    max_wait = 1


