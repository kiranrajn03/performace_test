from datetime import datetime
import logging
import multiprocessing
import os
import threading

from locust import events
import pika
from pika.exceptions import AMQPError

LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
LOGGER = logging.getLogger(__name__)


class RabbitMQClient(object):
    _connected = False

    def __init__(self):
        self._process_name = multiprocessing.current_process().name
        self._thread_name = threading.current_thread().name

    def connect(self):
        print("entering connect")
        params = pika.URLParameters('amqp://wes-dev-user:wes-dev-user@oh0lw0112.intellig.local:5672/#/queues/%2F/')
        self._connection = pika.BlockingConnection(params)
        print("after pika blockingconnect")
        self._channel = self._connection.channel()
        print("after connection channel")
        self._connected = True
        print("connection established")

    def publish(self):
        """
        Constructs and publishes a simple message
        via amqp.basic_publish

        """
        if not self._connected:
            self.connect()

        # message = "Process: {}, Thread: {}".format(self._process_name,
        #                                          self._thread_name)
        message = 'RequestRouteData|ContainerID=T88923|ScannerID=336|ContainerWeight=10.0|ContainerLength=10.0|ContainerWidth=10.0|ContainerHeight=10.0|SizingValidation=2'
        message1 = 'RequestRouteData|ContainerID=T22234|ScannerID=336|ContainerWeight=10.0|ContainerLength=10.0|ContainerWidth=10.0|ContainerHeight=10.0|SizingValidation=2'
        message2 = 'RequestRouteData|ContainerID=T212331|ScannerID=336|ContainerWeight=10.0|ContainerLength=10.0|ContainerWidth=10.0|ContainerHeight=10.0|SizingValidation=2'
        message3 = 'RequestRouteData|ContainerID=T212332|ScannerID=336|ContainerWeight=10.0|ContainerLength=10.0|ContainerWidth=10.0|ContainerHeight=10.0|SizingValidation=2'
        message4 = 'RequestRouteData|ContainerID=T999981|ScannerID=336|ContainerWeight=10.0|ContainerLength=10.0|ContainerWidth=10.0|ContainerHeight=10.0|SizingValidation=2'
        message5 = 'RequestRouteData|ContainerID=T999982|ScannerID=336|ContainerWeight=10.0|ContainerLength=10.0|ContainerWidth=10.0|ContainerHeight=10.0|SizingValidation=2'
        message6 = 'RequestRouteData|ContainerID=T999983|ScannerID=336|ContainerWeight=10.0|ContainerLength=10.0|ContainerWidth=10.0|ContainerHeight=10.0|SizingValidation=2'
        message7 = 'RequestRouteData|ContainerID=T999984|ScannerID=336|ContainerWeight=10.0|ContainerLength=10.0|ContainerWidth=10.0|ContainerHeight=10.0|SizingValidation=2'
        try:
            watch = StopWatch()
            watch.start()
            self._channel.basic_publish(exchange='', routing_key='bombbay-gto-tp.Routing', body=message,
                                        properties=pika.BasicProperties(content_type='text/plain',
                                                                        delivery_mode=1))
            self._channel.basic_publish(exchange='', routing_key='bombbay-gto-tp.Routing', body=message1,
                                        properties=pika.BasicProperties(content_type='text/plain',
                                                                        delivery_mode=1))
            self._channel.basic_publish(exchange='', routing_key='bombbay-gto-tp.Routing', body=message2,
                                        properties=pika.BasicProperties(content_type='text/plain',
                                                                        delivery_mode=1))
            self._channel.basic_publish(exchange='', routing_key='bombbay-gto-tp.Routing', body=message3,
                                        properties=pika.BasicProperties(content_type='text/plain',
                                                                        delivery_mode=1))
            self._channel.basic_publish(exchange='', routing_key='bombbay-gto-tp.Routing', body=message4,
                                        properties=pika.BasicProperties(content_type='text/plain',
                                                                        delivery_mode=1))
            self._channel.basic_publish(exchange='', routing_key='bombbay-gto-tp.Routing', body=message5,
                                        properties=pika.BasicProperties(content_type='text/plain',
                                                                        delivery_mode=1))
            self._channel.basic_publish(exchange='', routing_key='bombbay-gto-tp.Routing', body=message6,
                                        properties=pika.BasicProperties(content_type='text/plain',
                                                                        delivery_mode=1))
            self._channel.basic_publish(exchange='', routing_key='bombbay-gto-tp.Routing', body=message7,
                                        properties=pika.BasicProperties(content_type='text/plain',
                                                                        delivery_mode=1))
            watch.stop()
        except AMQPError as e:
            watch.stop()
            events.request_failure.fire(
                request_type="BASIC_PUBLISH",
                name="test.message",
                response_time=watch.elapsed_time(),
                exception=e,
            )
        else:
            events.request_success.fire(
                request_type="BASIC_PUBLISH",
                name="test.message",
                response_time=watch.elapsed_time(),
                response_length=0
            )

    def close_channel(self):
        if self._channel is not None:
            LOGGER.info('Closing the channel')
            self._channel.close()

    def close_connection(self):
        if self._connection is not None:
            LOGGER.info('Closing connection')
            self._connection.close()

    def disconnect(self):
        self.close_channel()
        self.close_connection()
        self._connected = False


class StopWatch():
    def start(self):
        self._start = datetime.now()

    def stop(self):
        self._end = datetime.now()

    def elapsed_time(self):
        timedelta = self._end - self._start
        return timedelta.total_seconds() * 1000


client = None


def get_client():
    global client
    if client is None:
        client = RabbitMQClient()
    return client
